#!/usr/bin/env python3

# The Finite Element Method, Chap. 2
# -(a*u')' = f

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np

np.set_printoptions(precision=2)

def StiffnessAssembler1D(x, a, kappa):
  n = len(x)
  A = np.zeros(shape=(n, n))
  elem = len(x) - 1
  for i in range(elem):
    x0 = x[i]
    x1 = x[i+1]
    h = x1 - x0
    xmid = (x0 + x1) / 2
    amid = a(xmid)
    val = amid/h
    A[i][i] += val
    A[i+1][i] += -val
    A[i][i+1] += -val
    A[i+1][i+1] += val
  # boundary
  A[0][0] += kappa[0]
  A[n-1][n-1] += kappa[1]
  return A

def LoadAssembler1D(x, f):
  n = len(x)
  elem = len(x) - 1
  b = np.zeros(n)
  for i in range(elem):
    x0 = x[i]
    x1 = x[i+1]
    h = x1 - x0
    b[i] += f(x0) * h / 2
    b[i+1] += f(x1) * h / 2
  return b

def SourceAssembler1D(x, f, kappa, g):
  b = LoadAssembler1D(x, f)
  b[0] += kappa[0] * g[0]
  b[-1] += kappa[1] * g[1]

x = np.linspace(0, 5, 10)
k = (1,1)

# Used in mid-point quadrature for the integral part, but what does it
# corresponds to in reality?
def func(x):
  return 10

A = StiffnessAssembler1D(x, func, k)

print("A:")
print(A)
cond = np.linalg.cond(A)
print(f"A cond {cond}")

Ai = np.linalg.inv(A)
print(Ai)
