import numpy as np

eps = 2.22e-16

np.set_printoptions(precision=2)

n = 10
a = np.zeros((n, n))
for i in range(n):
  for j in range(n):
    a[i][j] = 1 / (i + j + 1)

cond = np.linalg.cond(a)
precision = cond * eps
ai = np.linalg.inv(a)

print("matrix:")
print(a)
print("inverse:")
print(ai)
print(f"condition number: {cond:g}")
print(f"precision: {precision:g}")
