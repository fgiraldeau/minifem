#include <Eigen/Dense>
#include <cmath>
#include <fstream>
#include <iostream>

typedef double (*func)(double x);
double my_func(double x) { return 2 * x * std::sin(2 * M_PI * x) + 3; }

void MassAssemble1D(Eigen::VectorXd &x, Eigen::MatrixXd &M) {
  M = Eigen::MatrixXd::Zero(x.size(), x.size());

  for (int i = 0; i < x.size() - 1; i++) {
    double h = x(i + 1) - x(i);
    M(i, i) += h / 3;
    M(i, i + 1) += h / 6;
    M(i + 1, i) += h / 6;
    M(i + 1, i + 1) += h / 3;
  }
}

void LoadAssemble1D(func f, Eigen::VectorXd &x, Eigen::VectorXd &b) {
  b = Eigen::VectorXd::Zero(x.size());

  for (int i = 0; i < x.size() - 1; i++) {
    double h = x(i + 1) - x(i);
    b(i) += f(x(i)) * h / 2;
    b(i + 1) += f(x(i + 1)) * h / 2;
  }
}

struct L2Projection {
  L2Projection(int _n, func _f) : n(_n), f(_f) {}

  void assemble() {
    x = Eigen::VectorXd::LinSpaced(n, 0.0, 1.0);
    y = x.unaryExpr(&my_func);

    MassAssemble1D(x, M);
    LoadAssemble1D(f, x, b);
  }

  void solve() {
    sol = M.colPivHouseholderQr().solve(b);
    err = y - sol;
  }

  double error() { return err.array().abs().sum(); }

  void dump() {
    std::cout << x << std::endl;
    std::cout << y << std::endl;
    std::cout << M << std::endl;
    std::cout << b << std::endl;
    std::cout << sol << std::endl;
    std::cout << err << std::endl;
  }

  int n;
  func f;

  Eigen::VectorXd x;
  Eigen::VectorXd y;

  Eigen::MatrixXd M;
  Eigen::VectorXd b;

  Eigen::VectorXd sol;
  Eigen::VectorXd err;
};

int main(int argc, char **argv) {

  {
    L2Projection l2(10, my_func);

    std::cout << "assembling" << std::endl;
    l2.assemble();
    l2.dump();

    std::cout << "solving" << std::endl;
    l2.solve();

    std::cout << "done" << std::endl;

    std::ofstream file("out.dat");
    if (file.is_open()) {
      for (ulong i = 0; i < l2.x.size(); i++) {
        file << l2.x(i) << ' ' << l2.y(i) << ' ' << l2.sol(i) << ' '
             << l2.err(i) << '\n';
      }
    }
  }

  // error convergence
  {
    std::ofstream file("err.dat");
    for (int i = 3; i <= 10; i++) {
      int n = 1 << i;
      std::cout << "computing " << n << std::endl;
      L2Projection l2(n, my_func);
      l2.assemble();
      l2.solve();
      double e = l2.error();
      file << n << ' ' << e << '\n';
    }
  }

  return 0;
}
