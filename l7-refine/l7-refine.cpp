#include <iostream>
#include <fstream>
#include <unordered_set>
#include <iomanip>
#include <algorithm>

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_refinement.h>

#include <deal.II/grid/tria_iterator.h>

#include <deal.II/numerics/data_out.h>

using namespace dealii;

template <int dim>
double dist(const Point<dim> &p)
{
    return std::abs(p.distance(Point<dim>()) - 1.0);
}

int main()
{
    Triangulation<2> triangulation;
    GridGenerator::hyper_cube(triangulation, -5.0, 5.0);
    triangulation.refine_global(2);

    std::cout << "   Number of active cells: " << triangulation.n_active_cells()
              << std::endl
              << "   Total number of cells: " << triangulation.n_cells()
              << std::endl;

    // refine

    for (int step = 0; step < 10; step++) {
        Triangulation<2>::active_cell_iterator ti  = triangulation.begin_active();

        Vector<double> dist_vector(triangulation.n_active_cells());
        std::vector<unsigned int> cell_idx(triangulation.n_active_cells());

        while(ti != triangulation.end()) {
            unsigned int idx = ti->active_cell_index();

            double d = dist(ti->barycenter());
            std::cout << idx << " " << d << std::endl;
            dist_vector[idx] = d;
            cell_idx[idx] = idx;
            ti++;
        }

        std::sort(cell_idx.begin(), cell_idx.end(), [&dist_vector](int i, int j) {
            return dist_vector[i] < dist_vector[j];
        });

        size_t to_resize = std::max(cell_idx.size() * 10 / 100, (size_t)10);
        to_resize = std::min(cell_idx.size(), to_resize);
        cell_idx.resize(to_resize);
        std::unordered_set<unsigned int> cell_idx_set(cell_idx.begin(), cell_idx.end());

        ti  = triangulation.begin_active();
        while(ti != triangulation.end()) {
            unsigned int idx = ti->active_cell_index();
            std::cout << idx << std::endl;
            if (cell_idx_set.find(idx) != cell_idx_set.end()) {
                ti->set_refine_flag();
            }
            ti++;
        }

        triangulation.execute_coarsening_and_refinement();
    }

    DataOut<2> data_out;
    data_out.attach_triangulation(triangulation);
    data_out.build_patches();

    std::ofstream output("mesh.vtk");
    data_out.write_vtk(output);

    return 0;
}
