import numpy as np
import random
import sys

np.set_printoptions(precision=2)

A = np.matrix([[1e6,2],[3,4e-6]])
B = np.linalg.inv(A)

I = np.matmul(A, B)
print(A)
print(B)
print(I)

A_c = np.linalg.cond(A)
print(A_c)

# access a col
print(A[:, 1])

# access a row
print(A[1, :])

def gen_rand_mat(n = 10):
    a = np.identity(n)

    for i in range(10):
        # row selectors
        src = random.randint(0, n-1)
        dst = random.randint(0, n-1)
        f = random.uniform(1, 2)
        # multiply and add
        a[dst, :] += a[src, :] * f

    return a

x = gen_rand_mat(10)
print(x)
print(np.linalg.inv(x))
print(np.linalg.cond(x))

sys.exit(0)

err = 0
for i in range(1000):
    x = gen_rand_mat(100)
    try:
        np.linalg.inv(x)
        sys.stdout.write(".")
    except np.linalg.linalg.LinAlgError:
        sys.stdout.write("E")
        err += 1

print(err)
